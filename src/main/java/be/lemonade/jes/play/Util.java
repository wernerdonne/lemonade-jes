package be.lemonade.jes.play;

import static akka.util.ByteString.fromString;
import static be.lemonade.jwt.Open.getPayload;
import static java.net.URLDecoder.decode;
import static net.pincette.util.Json.string;
import static net.pincette.util.Util.tryToGet;

import be.lemonade.jwt.Key;
import be.lemonade.jwt.Open;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.interfaces.Verification;
import com.typesafe.config.Config;
import java.util.Optional;
import javax.json.JsonStructure;
import net.pincette.util.Util.GeneralException;
import play.http.HttpEntity;
import play.mvc.Http.RequestHeader;
import play.mvc.Result;
import play.mvc.Results;

public class Util {

  private Util() {}

  public static JWTVerifier createJwtVerifier(final Config config) {
    return Optional.ofNullable(config.getString("be.lemonade.jes.jwtRSAPublicKey"))
        .flatMap(Key::getKey)
        .flatMap(Key::getRSAPublicKey)
        .map(Open::withRsa)
        .map(Verification::build)
        .orElseThrow(() -> new GeneralException("Can't create JWT verifier"));
  }

  public static Optional<String> getBearerToken(final RequestHeader request) {
    final Optional<String> token =
        request
            .header("Authorization")
            .map(header -> header.split(" "))
            .filter(s -> s.length == 2)
            .filter(s -> s[0].equalsIgnoreCase("Bearer"))
            .map(s -> s[1]);

    return (token.isPresent() ? token : Optional.ofNullable(request.getQueryString("access_token")))
        .flatMap(t -> tryToGet(() -> decode(t, "UTF-8")));
  }

  public static Optional<String> getUser(final String jwt, final JWTVerifier verifier) {
    return getPayload(jwt, verifier)
        .filter(json -> json.containsKey("sub"))
        .map(json -> json.getString("sub"));
  }

  public static Result writeJson(final JsonStructure json) {
    return writeJson(json, 200);
  }

  public static Result writeJson(final JsonStructure json, final int status) {
    return Results.status(status)
        .sendEntity(
            new HttpEntity.Strict(fromString(string(json, true)), Optional.of("application/json")))
        .withHeader("Cache-Control", "no-cache, max-age=0, must-revalidate, no-store");
  }
}
