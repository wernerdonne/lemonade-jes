package be.lemonade.jes.play;

import static akka.pattern.Patterns.ask;
import static akka.stream.OverflowStrategy.fail;
import static be.lemonade.jes.akka.Commands.DELETE;
import static be.lemonade.jes.akka.Commands.GET;
import static be.lemonade.jes.akka.Commands.PATCH;
import static be.lemonade.jes.akka.Commands.PUT;
import static be.lemonade.jes.akka.JsonFields.COMMAND;
import static be.lemonade.jes.akka.JsonFields.ID;
import static be.lemonade.jes.akka.JsonFields.JWT;
import static be.lemonade.jes.akka.JsonFields.TYPE;
import static be.lemonade.jes.akka.Util.isDeleted;
import static be.lemonade.jes.kafka.Util.createReliableProducer;
import static be.lemonade.jes.kafka.Util.getProducerConfig;
import static be.lemonade.jes.play.Util.createJwtVerifier;
import static be.lemonade.jes.play.Util.getBearerToken;
import static be.lemonade.jes.play.Util.getUser;
import static be.lemonade.jes.play.Util.writeJson;
import static java.util.UUID.randomUUID;
import static java.util.concurrent.CompletableFuture.completedFuture;
import static javax.json.Json.createObjectBuilder;
import static net.pincette.util.Collections.map;
import static net.pincette.util.Collections.set;
import static net.pincette.util.Json.hasErrors;
import static net.pincette.util.Json.string;
import static net.pincette.util.Pair.pair;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.kafka.ConsumerSettings;
import akka.kafka.Subscriptions;
import akka.stream.Materializer;
import akka.stream.javadsl.Flow;
import akka.stream.javadsl.Source;
import be.lemonade.jes.akka.JsonAggregateRootRef;
import be.lemonade.jes.kafka.JsonConsumerActor;
import com.auth0.jwt.JWTVerifier;
import com.typesafe.config.Config;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.UnaryOperator;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonValue;
import net.pincette.util.Json;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import play.libs.EventSource;
import play.libs.F;
import play.libs.concurrent.HttpExecutionContext;
import play.libs.streams.ActorFlow;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import play.mvc.WebSocket;
import scala.compat.java8.FutureConverters;

/**
 * The REST API for JSON aggregate roots. The data is the API.
 *
 * @author Werner Donn\u00e9
 */
@Singleton
public class JsonAggregateController extends Controller {

  private static final String UTF8 = "UTF-8";

  private final ActorSystem actorSystem;
  private final String commandTopic;
  private final Config config;
  private final ActorRef dispatcher;
  private final HttpExecutionContext httpContext;
  private final Materializer materializer;
  private final JWTVerifier jwtVerifier;
  private KafkaProducer<String, String> commandProducer;

  @Inject
  public JsonAggregateController(
      final HttpExecutionContext httpContext,
      final JsonAggregateRootRef rootRef,
      final ActorSystem actorSystem,
      final Materializer materializer,
      final Config config) {
    this.httpContext = httpContext;
    dispatcher = rootRef.actorRef();
    this.actorSystem = actorSystem;
    this.materializer = materializer;
    this.config = config;
    jwtVerifier = createJwtVerifier(config);
    commandProducer =
        createReliableProducer(
            getProducerConfig(config), new StringSerializer(), new StringSerializer());
    commandTopic = config.getString("be.lemonade.jes.kafka.command.topic");
    startCommandConsumer();
  }

  private static JsonObjectBuilder initialBuilder(
      final JsonObject json, final String type, final String id, final String jwt) {
    final JsonObjectBuilder builder =
        createObjectBuilder(json).add(ID, id.toLowerCase()).add(TYPE, type).remove(JWT);

    return jwt != null ? builder.add(JWT, jwt) : builder;
  }

  @SuppressWarnings("squid:S1604") // Can't use lambda because of signature of create.
  private Flow<String, String, ?> createWebSocketActor(final String jwt) {
    return ActorFlow.actorRef(
        client ->
            Props.create(
                WebSocketActor.class,
                client,
                jwt,
                new Consumer<JsonObject>() {
                  public void accept(final JsonObject command) {
                    writeCommand(command);
                  }
                },
                config),
        16,
        fail(),
        actorSystem,
        materializer);
  }

  private CompletionStage<Boolean> dispatchCommand(final JsonObject command) {
    return FutureConverters.toJava(ask(dispatcher, command, 5000)).thenApply(reply -> true);
  }

  @BodyParser.Of(BodyParser.Bytes.class)
  public CompletionStage<Result> delete(final String type, final String id) {
    return send(type, id, b -> b.add(COMMAND, DELETE));
  }

  public CompletionStage<Result> get(final String type, final String id) {
    final JsonObjectBuilder builder =
        createObjectBuilder().add(ID, id.toLowerCase()).add(COMMAND, GET).add(TYPE, type);

    return FutureConverters.toJava(
            ask(
                dispatcher,
                getBearerToken(request())
                    .map(jwt -> builder.add(JWT, jwt).build())
                    .orElse(builder.build()),
                5000))
        .thenApply(reply -> (JsonObject) reply)
        .thenApply(reply -> isDeleted(reply) ? notFound() : writeJson(reply));
  }

  @BodyParser.Of(BodyParser.Bytes.class)
  public CompletionStage<Result> patch(final String type, final String id) {
    return send(type, id, b -> b.add(COMMAND, PATCH));
  }

  @BodyParser.Of(BodyParser.Bytes.class)
  public CompletionStage<Result> post(final String type, final String id) {
    return send(type, id, b -> b);
  }

  @BodyParser.Of(BodyParser.Bytes.class)
  public CompletionStage<Result> put(final String type, final String id) {
    return send(type, id, b -> b.add(COMMAND, PUT));
  }

  private CompletionStage<Result> send(
      final String type, final String id, final UnaryOperator<JsonObjectBuilder> builder) {
    final Optional<String> jwt = getBearerToken(request());
    final Function<JsonObject, Integer> result = json -> hasErrors(json) ? 409 : 200;
    final Function<JsonObject, CompletionStage<Result>> askAndWait =
        json ->
            FutureConverters.toJava(
                    ask(
                        dispatcher,
                        builder.apply(initialBuilder(json, type, id, jwt.orElse(null))).build(),
                        5000))
                .thenApply(reply -> (JsonObject) reply)
                .thenApply(
                    reply -> isDeleted(reply) ? notFound() : writeJson(reply, result.apply(reply)));
    final Function<JsonObject, CompletionStage<Result>> fireAndForget =
        json ->
            writeCommand(builder.apply(initialBuilder(json, type, id, jwt.orElse(null))).build())
                .thenApply(r -> ok());
    final Optional<String> user = jwt.flatMap(j -> getUser(j, jwtVerifier));

    return CompletableFuture.supplyAsync(
            () -> request().body().asBytes().decodeString(UTF8), httpContext.current())
        .thenApplyAsync(Json::from)
        .thenComposeAsync(
            json ->
                json.filter(Json::isObject)
                    .map(JsonValue::asJsonObject)
                    .map(
                        j ->
                            user.map(u -> fireAndForget.apply(j))
                                .orElseGet(() -> askAndWait.apply(j)))
                    .orElse(completedFuture(badRequest())));
  }

  public CompletionStage<Result> serverSentEvents() {
    final String user =
        getBearerToken(request()).flatMap(j -> getUser(j, jwtVerifier)).orElse(null);

    if (user == null) {
      return completedFuture(forbidden());
    }

    final Source<EventSource.Event, ?> eventSource =
        akka.kafka.javadsl.Consumer.plainSource(
                ConsumerSettings.create(
                        actorSystem, new StringDeserializer(), new StringDeserializer())
                    .withBootstrapServers(
                        config.getString("be.lemonade.jes.kafka.config.bootstrap.servers"))
                    .withGroupId(randomUUID().toString()), // Unique group per session.
                Subscriptions.topics(user))
            .map(ConsumerRecord::value)
            .map(EventSource.Event::event);

    return completedFuture(
        ok().chunked(eventSource.via(EventSource.flow())).as(Http.MimeTypes.EVENT_STREAM));
  }

  public WebSocket socket() {
    return WebSocket.Text.acceptOrResult(
        request -> {
          final Optional<String> jwt = getBearerToken(request());
          final String user = jwt.flatMap(j -> getUser(j, jwtVerifier)).orElse(null);

          return // Using Optional here causes type inference problems.
          user != null
              ? completedFuture(F.Either.Right(createWebSocketActor(jwt.orElse(null))))
              : completedFuture(F.Either.Left(forbidden()));
        });
  }

  private void startCommandConsumer() {
    actorSystem.actorOf(
        Props.create(
            JsonConsumerActor.class,
            set(commandTopic),
            new JsonConsumerActor.Config(
                (key, command) -> dispatchCommand(command),
                config.getConfig("be.lemonade.jes"),
                map(pair("group.id", config.getString("be.lemonade.jes.kafka.command.groupId"))))),
        "command");
  }

  private CompletionStage<Boolean> writeCommand(final JsonObject command) {
    return be.lemonade.jes.kafka.Util.send(
        commandProducer,
        new ProducerRecord<>(commandTopic, command.getString(ID), string(command)));
  }
}
