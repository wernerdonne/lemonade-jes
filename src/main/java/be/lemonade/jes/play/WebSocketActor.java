package be.lemonade.jes.play;

import static be.lemonade.jes.akka.JsonFields.ID;
import static be.lemonade.jes.akka.JsonFields.JWT;
import static be.lemonade.jes.akka.JsonFields.JWT_SUB;
import static be.lemonade.jes.play.Util.createJwtVerifier;
import static be.lemonade.jwt.Open.getPayload;
import static java.util.UUID.randomUUID;
import static java.util.concurrent.CompletableFuture.completedFuture;
import static javax.json.Json.createObjectBuilder;
import static net.pincette.util.Collections.map;
import static net.pincette.util.Collections.set;
import static net.pincette.util.Json.from;
import static net.pincette.util.Json.string;
import static net.pincette.util.Pair.pair;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.actor.Props;
import be.lemonade.jes.kafka.JsonConsumerActor;
import com.typesafe.config.Config;
import java.util.concurrent.CompletionStage;
import java.util.function.Consumer;
import javax.json.JsonObject;
import javax.json.JsonValue;
import net.pincette.util.Json;
import net.pincette.util.Util.GeneralException;

/**
 * Dispatches client requests. Replies coming from Kafka are sent back to the client. The
 * configuration for the Kafka consumer is taken in <code>be.lemonade
 * .jes</code>
 *
 * @author Werner Donn\u00e9
 */
public class WebSocketActor extends AbstractActor {

  private final ActorRef client;
  private final Consumer<JsonObject> dispatcher;
  private final String jwt;

  public WebSocketActor(
      final ActorRef client,
      final String jwt,
      final Consumer<JsonObject> dispatcher,
      final Config config) {
    this.client = client;
    this.jwt = jwt;
    this.dispatcher = dispatcher;

    context()
        .actorOf(
            Props.create(
                JsonConsumerActor.class,
                set(
                    getPayload(jwt, createJwtVerifier(config))
                        .map(j -> j.getString(JWT_SUB))
                        .orElseThrow(() -> new GeneralException("JWT token without sub"))),
                new JsonConsumerActor.Config(
                    (k, v) -> replyProcessor(v),
                    config.getConfig("be.lemonade.jes"),
                    map(pair("group.id", randomUUID().toString())) // Unique group per session.
                    )));
  }

  public Receive createReceive() {
    return receiveBuilder().match(String.class, this::handleRequest).build();
  }

  private void handleRequest(final String request) {
    from(request)
        .filter(Json::isObject)
        .map(JsonValue::asJsonObject)
        .ifPresent(
            json ->
                dispatcher.accept(
                    createObjectBuilder(json)
                        .add(ID, json.getString(ID).toLowerCase())
                        .add(JWT, jwt)
                        .build()));
  }

  private CompletionStage<Boolean> replyProcessor(final JsonObject message) {
    client.tell(string(message), self());

    return completedFuture(true);
  }
}
