package be.lemonade.jes.kafka;

import static java.lang.Integer.MAX_VALUE;
import static java.util.Collections.unmodifiableMap;
import static net.pincette.util.Collections.map;
import static net.pincette.util.Pair.pair;

import java.util.Map;

public class ReliableProducer {

  public static final Map<String, Object> CONFIG =
      unmodifiableMap(
          map(
              pair("acks", "all"),
              pair("enable.idempotence", true),
              pair("max.block.ms", Long.MAX_VALUE),
              pair("max.in.flight.requests.per.connection", 1),
              pair("retries", MAX_VALUE)));

  private ReliableProducer() {}
}
