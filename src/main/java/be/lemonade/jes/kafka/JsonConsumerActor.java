package be.lemonade.jes.kafka;

import static java.util.concurrent.CompletableFuture.completedFuture;
import static net.pincette.util.Collections.map;
import static net.pincette.util.Collections.merge;
import static net.pincette.util.Json.from;
import static net.pincette.util.Pair.pair;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CompletionStage;
import javax.json.JsonValue;
import net.pincette.util.Json;

/**
 * This consumer passes only <code>JsonObject</code> messages to the given processor. Everything
 * else is ignored.
 *
 * @author Werner Donn\u00e9
 */
public class JsonConsumerActor extends ReliableConsumerActor<String, String> {

  public JsonConsumerActor(final Set<String> topics, final Config config) {
    super(
        topics,
        new ReliableConsumerActor.Config<>(
            (k, v) -> process(k, v, config.processor),
            config.configuration,
            merge(
                map(
                    pair(
                        "key.deserializer",
                        "org.apache.kafka.common.serialization.StringDeserializer"),
                    pair(
                        "value.deserializer",
                        "org.apache.kafka.common.serialization.StringDeserializer")),
                config.extraKafkaConfig != null ? config.extraKafkaConfig : new HashMap<>())));
  }

  private static CompletionStage<Boolean> process(
      final String key, final String value, final JsonConsumer processor) {
    return from(value)
        .filter(Json::isObject)
        .map(JsonValue::asJsonObject)
        .map(json -> processor.apply(key, json))
        .orElse(completedFuture(true)); // Ignore messages that are not JSON objects.
  }

  public static class Config {

    private final com.typesafe.config.Config configuration;
    private final Map<String, Object> extraKafkaConfig;
    private final JsonConsumer processor;

    public Config(
        final JsonConsumer processor,
        final com.typesafe.config.Config config,
        final Map<String, Object> extraKafkaConfig) {
      this.processor = processor;
      this.configuration = config;
      this.extraKafkaConfig = extraKafkaConfig;
    }
  }
}
