package be.lemonade.jes.kafka;

import static be.lemonade.jes.kafka.Util.getConfig;
import static java.time.Duration.ofMillis;
import static java.util.concurrent.CompletableFuture.completedFuture;
import static net.pincette.util.Collections.map;
import static net.pincette.util.Collections.merge;
import static net.pincette.util.Pair.pair;
import static net.pincette.util.StreamUtil.stream;
import static net.pincette.util.Util.tryToGet;

import akka.actor.AbstractActor;
import java.time.Duration;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.TimeUnit;
import java.util.function.BiFunction;
import net.pincette.function.SideEffect;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.consumer.OffsetAndMetadata;
import org.apache.kafka.common.TopicPartition;

/**
 * This is a Kafka consumer wrapped in an Akka actor. The consumer will commit the offset only when
 * the given processor returns <code>true</code>.
 *
 * @param <K> the key type.
 * @param <V> the value type.
 * @author Werner Donn\u00e9
 */
public class ReliableConsumerActor<K, V> extends AbstractActor {

  private static final String POLL = "poll";

  private final Duration backoffTime;
  private final KafkaConsumer<K, V> consumer;
  private final Duration pollInterval;
  private final BiFunction<K, V, CompletionStage<Boolean>> processor;

  /**
   * Creates the consumer and subscribes to it.
   *
   * @param topics the topics that should be consumed.
   */
  public ReliableConsumerActor(final Set<String> topics, final Config<K, V> config) {
    this.processor = config.processor;
    this.pollInterval =
        tryToGet(() -> ofMillis(config.configuration.getLong("kafka.pollInterval")))
            .orElse(ofMillis(10));
    this.backoffTime =
        tryToGet(() -> ofMillis(config.configuration.getLong("kafka.backoffTime")))
            .orElse(ofMillis(500));

    consumer =
        new KafkaConsumer<>(
            merge(
                getConfig(config.configuration, "kafka.config"),
                config.extraKafkaConfig,
                map(pair("enable.auto.commit", false))));
    consumer.subscribe(topics);
  }

  private static Map<TopicPartition, OffsetAndMetadata> getOffset(final ConsumerRecord record) {
    return map(
        pair(
            new TopicPartition(record.topic(), record.partition()),
            new OffsetAndMetadata(record.offset() + 1)));
  }

  private boolean commit(final ConsumerRecord record) {
    return tryToGet(
            () -> {
              consumer.commitSync(getOffset(record));

              return true;
            })
        .orElse(false);
  }

  public Receive createReceive() {
    return receiveBuilder().matchEquals(POLL, message -> handlePoll()).build();
  }

  private void handlePoll() {
    // Don't block, because this occupies a thread. The poll interval is the
    // extra latency one is prepared to accept. This approach can be changed
    // as soon as there is an asynchronous non-blocking Kafka-consumer.

    next(
        stream(consumer.poll(0).iterator())
            .map(
                record ->
                    processor
                        .apply(record.key(), record.value())
                        .thenApply(result -> pair(record, result)))
            .reduce(
                (c1, c2) ->
                    c1.thenComposeAsync(result -> result.second ? c2 : completedFuture(result)))
            .flatMap(stage -> tryToGet(() -> stage.toCompletableFuture().get()))
            .map(
                result ->
                    !result.second || !commit(result.first)
                        ? SideEffect.<Duration>run(() -> rewind(result.first))
                            .andThenGet(() -> backoffTime)
                        : pollInterval)
            .orElse(pollInterval));
  }

  private void next(final Duration duration) {
    context()
        .system()
        .scheduler()
        .scheduleOnce(
            scala.concurrent.duration.Duration.create(duration.toMillis(), TimeUnit.MILLISECONDS),
            this::poll,
            context().system().dispatcher());
  }

  private void poll() {
    self().tell(POLL, self());
  }

  @Override
  public void postStop() {
    consumer.close();
  }

  @Override
  public void preStart() {
    poll();
  }

  private void rewind(final ConsumerRecord record) {
    consumer.seek(new TopicPartition(record.topic(), record.partition()), record.offset());
  }

  public static class Config<K, V> {

    private final com.typesafe.config.Config configuration;
    private final Map<String, Object> extraKafkaConfig;
    private final BiFunction<K, V, CompletionStage<Boolean>> processor;

    /**
     * The configuration needed to create the actor.
     *
     * @param processor the function that should return <code>true</code> in order for the offset to
     *     be committed.
     * @param config the Kafka configuration. The path "kafka.pollInterval" is the interval in
     *     milliseconds between two polls. At the Kafka level the poll will have no timeout and
     *     hence return immediately. This avoids blocking. The <code>pollInterval</code> is the
     *     maximum latency one is prepared to accept. The default value is 10ms. The path
     *     "kafka.backoffTime" is the time in milliseconds after a message processing failure. The
     *     default value is 500ms. The path "kafka.config" is the general Kafka configuration.
     * @param extraKafkaConfig extra configurations for Kafka.
     */
    public Config(
        final BiFunction<K, V, CompletionStage<Boolean>> processor,
        final com.typesafe.config.Config config,
        final Map<String, Object> extraKafkaConfig) {
      this.processor = processor;
      this.configuration = config;
      this.extraKafkaConfig = extraKafkaConfig != null ? extraKafkaConfig : new HashMap<>();
    }
  }
}
