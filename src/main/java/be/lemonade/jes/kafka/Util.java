package be.lemonade.jes.kafka;

import static java.util.stream.Collectors.toMap;
import static net.pincette.util.Collections.merge;

import com.typesafe.config.Config;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.Serializer;

/**
 * Some Kafka utilities.
 *
 * @author Werner Donn\u00e9
 */
public class Util {

  private Util() {}

  public static <K, V> KafkaProducer<K, V> createReliableProducer(
      final Map<String, Object> config,
      final Serializer<K> keySerializer,
      final Serializer<V> valueSerializer) {
    return new KafkaProducer<>(
        merge(config, ReliableProducer.CONFIG), keySerializer, valueSerializer);
  }

  /**
   * Gets the configuration object at <code>path</code> in <code>config</code> and flattens the tree
   * under it so that the keys in the resulting map are dot-separated paths as Kafka expects it.
   *
   * @param config the given configuration object.
   * @param path the dot-separated path withing the configuration object.
   */
  public static Map<String, Object> getConfig(final Config config, final String path) {
    return config
        .getConfig(path)
        .entrySet()
        .stream()
        .collect(toMap(Map.Entry::getKey, e -> e.getValue().unwrapped()));
  }

  /**
   * Gets the general Kafka configuration at the path <code>be.lemonade.jes.kafka.config</code>.
   *
   * @param config the given configuration object.
   */
  public static Map<String, Object> getConfig(final Config config) {
    return getConfig(config, "be.lemonade.jes.kafka.config");
  }

  /**
   * Gets the general Kafka producer configuration at the path <code>be.lemonade.jes.kafka
   * .producer</code>, merged with the general Kafka configuration at the path <code>be
   * .lemonade.jes.kafka.config</code>.
   *
   * @param config the given configuration object.
   */
  public static Map<String, Object> getProducerConfig(final Config config) {
    return merge(getConfig(config), getConfig(config, "be.lemonade.jes.kafka.producer"));
  }

  public static <K, V> CompletionStage<Boolean> send(
      final KafkaProducer<K, V> producer, final ProducerRecord<K, V> record) {
    final CompletableFuture<Boolean> completableFuture = new CompletableFuture<>();

    producer.send(record, (metadata, exception) -> completableFuture.complete(true));

    return completableFuture;
  }
}
