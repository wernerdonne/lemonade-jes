package be.lemonade.jes.bson;

import static java.time.Instant.ofEpochMilli;
import static java.time.Instant.ofEpochSecond;
import static javax.json.Json.createArrayBuilder;
import static javax.json.Json.createObjectBuilder;
import static javax.json.Json.createValue;
import static net.pincette.util.Json.asNumber;
import static net.pincette.util.Json.asString;

import java.nio.ByteBuffer;
import java.util.stream.Collectors;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonNumber;
import javax.json.JsonObject;
import javax.json.JsonString;
import javax.json.JsonValue;
import org.bson.BsonArray;
import org.bson.BsonBinaryReader;
import org.bson.BsonBinaryWriter;
import org.bson.BsonBoolean;
import org.bson.BsonDateTime;
import org.bson.BsonDocument;
import org.bson.BsonDouble;
import org.bson.BsonInt32;
import org.bson.BsonInt64;
import org.bson.BsonNull;
import org.bson.BsonNumber;
import org.bson.BsonString;
import org.bson.BsonTimestamp;
import org.bson.BsonValue;
import org.bson.codecs.BsonValueCodec;
import org.bson.codecs.DecoderContext;
import org.bson.codecs.EncoderContext;
import org.bson.io.BasicOutputBuffer;
import org.bson.io.OutputBuffer;

/**
 * BSON utilities.
 *
 * @author Werner Donn\u00e9
 */
public class Bson

{

  private Bson() {
  }

  public static JsonValue
  fromBson(final BsonValue bson) {
    switch (bson.getBsonType()) {
      case ARRAY:
        return fromBson(bson.asArray());
      case BOOLEAN:
        return bson.asBoolean().getValue() ? JsonValue.TRUE : JsonValue.FALSE;
      case DATE_TIME:
        return fromBson(bson.asDateTime());
      case DOCUMENT:
        return fromBson(bson.asDocument());
      case DOUBLE:
        return fromBson(bson.asDouble());
      case INT32:
        return fromBson(bson.asInt32());
      case INT64:
        return fromBson(bson.asInt64());
      case NULL:
        return JsonValue.NULL;
      case STRING:
        return fromBson(bson.asString());
      case TIMESTAMP:
        return fromBson(bson.asTimestamp());
      default:
        return null;
    }
  }

  public static JsonArray
  fromBson(final BsonArray array) {
    return
        array
            .stream()
            .map(Bson::fromBson)
            .reduce(createArrayBuilder(), JsonArrayBuilder::add, (b1, b2) -> b1)
            .build();
  }

  public static JsonObject
  fromBson(final BsonDocument bson) {
    return
        bson
            .entrySet()
            .stream()
            .reduce(
                createObjectBuilder(),
                (b, e) -> b.add(e.getKey(), fromBson(e.getValue())),
                (b1, b2) -> b1
            )
            .build();
  }

  public static JsonNumber
  fromBson(final BsonInt32 bson) {
    return createValue(bson.getValue());
  }

  public static JsonNumber
  fromBson(final BsonInt64 bson) {
    return createValue(bson.getValue());
  }

  public static JsonNumber
  fromBson(final BsonDouble bson) {
    return createValue(bson.getValue());
  }

  public static JsonString
  fromBson(final BsonDateTime bson) {
    return createValue(ofEpochMilli(bson.getValue()).toString());
  }

  public static JsonString
  fromBson(final BsonString bson) {
    return createValue(bson.getValue());
  }

  public static JsonString
  fromBson(final BsonTimestamp bson) {
    return createValue(ofEpochSecond(bson.getTime()).toString());
  }

  public static BsonValue
  fromBytes(final byte[] array) {
    return
        new BsonValueCodec()
            .decode(new BsonBinaryReader(ByteBuffer.wrap(array)), DecoderContext.builder().build());
  }

  public static BsonValue
  fromJson(final JsonValue json) {
    switch (json.getValueType()) {
      case ARRAY:
        return fromJson(json.asJsonArray());
      case FALSE:
        return BsonBoolean.FALSE;
      case NULL:
        return BsonNull.VALUE;
      case NUMBER:
        return fromJson(asNumber(json));
      case OBJECT:
        return fromJson(json.asJsonObject());
      case STRING:
        return fromJson(asString(json));
      case TRUE:
        return BsonBoolean.TRUE;
      default:
        return null;
    }
  }

  public static BsonArray
  fromJson(final JsonArray array) {
    return new BsonArray(array.stream().map(Bson::fromJson).collect(Collectors.toList()));
  }

  public static BsonDocument
  fromJson(final JsonObject json) {
    return
        json
            .entrySet()
            .stream()
            .reduce(
                new BsonDocument(),
                (d, e) -> d.append(e.getKey(), fromJson(e.getValue())),
                (d1, d2) -> d1
            );
  }

  public static BsonNumber
  fromJson(final JsonNumber json) {
    return json.isIntegral() ? new BsonInt64(json.longValue()) : new BsonDouble(json.doubleValue());
  }

  public static BsonString
  fromJson(final JsonString json) {
    return new BsonString(json.getString());
  }

  public static byte[]
  toBytes(final BsonValue value) {
    final OutputBuffer out = new BasicOutputBuffer();

    new BsonValueCodec().encode(new BsonBinaryWriter(out), value, EncoderContext.builder().build());

    return out.toByteArray();
  }

} // Bson
