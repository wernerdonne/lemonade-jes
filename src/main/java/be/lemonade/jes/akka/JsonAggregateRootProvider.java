package be.lemonade.jes.akka;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.typesafe.config.Config;

/**
 * The provider for the aggregate root dispatcher dependency injection.
 *
 * @author Werner Donn\u00e9
 */
public class JsonAggregateRootProvider implements Provider<JsonAggregateRootRef> {

  private final ActorRef dispatcher;

  @Inject
  public JsonAggregateRootProvider(
      final JsonProcessorRegistration processors, final ActorSystem system, final Config config) {
    dispatcher =
        system.actorOf(
            Props.create(JsonAggregateRootDispatcher.class, processors, config), "dispatcher");
  }

  @Override
  public JsonAggregateRootRef get() {
    return new JsonAggregateRootRef(dispatcher);
  }
}
