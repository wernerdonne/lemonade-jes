package be.lemonade.jes.akka;

import static be.lemonade.jes.akka.JsonFields.TYPE;
import static java.util.stream.Collectors.toMap;
import static net.pincette.util.Pair.pair;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.actor.Props;
import akka.cluster.sharding.ClusterSharding;
import akka.cluster.sharding.ClusterShardingSettings;
import com.typesafe.config.Config;
import java.util.Map;
import java.util.Optional;
import javax.json.JsonObject;

/**
 * Creates cluster shards for the configured aggregate types and dispatches messages to the shards
 * based on the types. Reducers and validators can be registered.
 *
 * <p>The configuration should provide a Kafka configuration under <code>
 * be.lemonade.jes.kafka.config</code> and the number <code>be.lemonade.jes
 * .maxNumberOfShards</code>, which has 10 as the default value.
 */
public class JsonAggregateRootDispatcher extends AbstractActor {

  private final Map<String, ActorRef> regions;

  public JsonAggregateRootDispatcher(
      final JsonProcessorRegistration processors, final Config config) {
    final ClusterShardingSettings settings = ClusterShardingSettings.create(context().system());
    final ClusterSharding sharding = ClusterSharding.get(context().system());

    regions =
        processors
            .types()
            .map(
                type ->
                    pair(
                        type,
                        sharding.start(
                            type,
                            Props.create(
                                JsonAggregateRoot.class,
                                type,
                                processors.getValidator(type),
                                processors.getReducer(type),
                                config),
                            settings,
                            new JsonMessageExtractor(
                                config.hasPath("be.lemonade.jes.maxNumberOfShards")
                                    ? config.getInt("be.lemonade.jes.maxNumberOfShards")
                                    : 10))))
            .collect(toMap(pair -> pair.first, pair -> pair.second));
  }

  public Receive createReceive() {
    return receiveBuilder().match(JsonObject.class, this::handleMessage).build();
  }

  private void handleMessage(final JsonObject message) {
    Optional.ofNullable(message.getString(TYPE, null))
        .map(regions::get)
        .ifPresent(region -> region.forward(message, context()));
  }
}
