package be.lemonade.jes.akka;

/**
 * Common commands.
 *
 * @author Werner Donn\u00e9
 */
public class Commands {

  public static final String DELETE = "delete";
  public static final String GET = "get";
  public static final String PASSIVATE = "passivate";
  public static final String PATCH = "patch";
  public static final String PUT = "put";

  private Commands() {}
}
