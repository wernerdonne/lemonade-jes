package be.lemonade.jes.akka;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.Singleton;

/**
 * The dependency injection module for the aggregate root dipatcher and the registration of the JSON
 * processors.
 *
 * @author Werner Donn\u00e9
 */
public class Module extends AbstractModule {

  @Override
  protected void configure() {
    bind(JsonAggregateRootRef.class).toProvider(JsonAggregateRootProvider.class);
  }

  @Provides
  @Singleton
  public JsonProcessorRegistration processorRegistration() {
    return new JsonProcessorRegistration();
  }
}
