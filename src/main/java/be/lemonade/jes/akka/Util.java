package be.lemonade.jes.akka;

import static be.lemonade.jes.akka.JsonFields.CORR;
import static be.lemonade.jes.akka.JsonFields.DELETED;
import static be.lemonade.jes.akka.JsonFields.ID;
import static be.lemonade.jes.akka.JsonFields.JWT;
import static be.lemonade.jes.akka.JsonFields.OPS;
import static be.lemonade.jes.akka.JsonFields.SEQ;
import static be.lemonade.jes.akka.JsonFields.TYPE;
import static java.util.UUID.randomUUID;
import static javax.json.Json.createArrayBuilder;
import static javax.json.Json.createDiff;
import static javax.json.Json.createObjectBuilder;
import static javax.json.Json.createPatch;
import static net.pincette.util.Json.remove;
import static net.pincette.util.Json.set;
import static net.pincette.util.Util.must;

import java.util.Optional;
import java.util.Set;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import net.pincette.function.ConsumerWithException;
import net.pincette.util.Collections;

/**
 * Utilities.
 *
 * @author Werner Donn\u00e9
 */
public class Util {

  private static final Set<String> TECHNICAL_EVENT_FIELDS =
      Collections.set(CORR, ID, TYPE, SEQ, JWT);

  private Util() {}

  public static JsonObject applyEvent(
      final JsonObject json, final JsonObject event, final ConsumerWithException<String> report) {
    final ConsumerWithException<JsonObject> reporter =
        report != null ? (e -> report.accept(sequenceErrorMessage(json, e))) : null;

    return !isEvent(event) || !sameManagedObject(json, event) || seen(json, event)
        ? json
        : set(
            createPatch(must(event, e -> isNext(json, e), reporter).getJsonArray(OPS)).apply(json),
            SEQ,
            event.getInt(SEQ));
  }

  public static JsonObject createEvent(
      final JsonObject oldState, final JsonObject newState, final JsonObject command) {
    final JsonObjectBuilder builder =
        createObjectBuilder()
            .add(CORR, command.getString(CORR, randomUUID().toString()))
            .add(ID, newState.getString(ID))
            .add(TYPE, newState.getString(TYPE))
            .add(SEQ, oldState.getInt(SEQ, -1) + 1)
            .add(
                OPS,
                createArrayBuilder(
                    createDiff(removeTechnical(oldState), removeTechnical(newState))
                        .toJsonArray()));

    return command.containsKey(JWT)
        ? builder.add(JWT, command.getString(JWT)).build()
        : builder.build();
  }

  private static boolean hasOps(final JsonObject event) {
    return Optional.ofNullable(event.getJsonArray(OPS)).filter(ops -> !ops.isEmpty()).isPresent();
  }

  public static boolean isDeleted(final JsonObject json) {
    return json.getBoolean(DELETED, false);
  }

  public static boolean isEvent(final JsonObject event) {
    return isManagedObject(event) && hasOps(event);
  }

  public static boolean isManagedObject(final JsonObject json) {
    return json.containsKey(ID) && json.containsKey(TYPE) && json.containsKey(SEQ);
  }

  public static boolean isNext(final JsonObject json, final JsonObject event) {
    return event.getInt(SEQ) == json.getInt(SEQ) + 1;
  }

  private static JsonObject removeTechnical(final JsonObject json) {
    return remove(json, TECHNICAL_EVENT_FIELDS);
  }

  public static boolean sameManagedObject(final JsonObject json1, final JsonObject json2) {
    return isManagedObject(json1)
        && isManagedObject(json2)
        && json1.getString(ID).equals(json2.getString(ID))
        && json1.getString(TYPE).equals(json2.getString(TYPE));
  }

  private static boolean seen(final JsonObject json, final JsonObject event) {
    return event.getInt(SEQ) <= json.getInt(SEQ);
  }

  public static String sequenceErrorMessage(final JsonObject json, final JsonObject event) {
    return "SEQUENCE ERROR: event received for ("
        + json.getString(ID)
        + ","
        + json.getString(TYPE)
        + ") with sequence number "
        + event.getInt(SEQ)
        + ", while "
        + (json.getInt(SEQ) + 1)
        + " was expected";
  }
}
