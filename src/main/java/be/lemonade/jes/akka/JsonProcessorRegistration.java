package be.lemonade.jes.akka;

import static java.util.concurrent.CompletableFuture.completedFuture;
import static java.util.stream.Stream.concat;
import static net.pincette.util.Pair.pair;

import be.lemonade.jes.kafka.JsonConsumer;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Stream;
import net.pincette.util.Pair;

/**
 * A singleton with which an application can register its validators and reducers.
 *
 * @author Werner Donn\u00e9
 */
public class JsonProcessorRegistration {

  private Map<Pair<String, String>, List<JsonConsumer>> consumers = new ConcurrentHashMap<>();
  private Map<String, JsonProcessor> reducers = new ConcurrentHashMap<>();
  private Map<String, JsonProcessor> validators = new ConcurrentHashMap<>();

  /**
   * Returns the reducer for <code>type</code>. If none was registered a reducer that just returns
   * the state will be returned.
   *
   * @param type the aggregate type.
   * @return The reducer.
   */
  public JsonProcessor getReducer(final String type) {
    return Optional.ofNullable(reducers.get(type)).orElse((s, c) -> completedFuture(s));
  }

  /**
   * Returns the validator for <code>type</code>. If none was registered a validator that just
   * returns the command will be returned, which means validation always succeeds.
   *
   * @param type the aggregate type.
   * @return The validator.
   */
  public JsonProcessor getValidator(final String type) {
    return Optional.ofNullable(validators.get(type)).orElse((s, c) -> completedFuture(c));
  }

  /**
   * Set the consumer for the given topic/groupId pair. The group size should be in the
   * configuration.
   *
   * @param topic the topic for which a Kafka consumer actor will be run.
   * @param groupId the groupId for the consumer group.
   * @param consumer the function that will be called when the consumer receives a message. This
   *     function receives the message key and the message. It should return <code>true</code> when
   *     processing was successful and <code>false</code> otherwise.
   */
  public void setConsumer(final String topic, final String groupId, final JsonConsumer consumer) {
    consumers.computeIfAbsent(pair(topic, groupId), k -> new ArrayList<>()).add(consumer);
  }

  /**
   * Sets the reducer for a given type. A reducer receives the current state of the aggregate and
   * the command. It should return the new state.
   *
   * @param type the aggregate type.
   * @param reducer the function that performs the reduction.
   * @return The new state of the aggregate.
   */
  public JsonProcessorRegistration setReducer(final String type, final JsonProcessor reducer) {
    reducers.put(type, reducer);

    return this;
  }

  /**
   * Sets the validator for a given type. A validator receives the current state of the aggregate
   * and the command. It should return the command, possibly with error annotations, in which case
   * the field <code>_error</code> should be set to <code>true</code>.
   *
   * @param type the aggregate type.
   * @param validator the function that performs the validation.
   * @return The command.
   */
  public JsonProcessorRegistration setValidator(final String type, final JsonProcessor validator) {
    validators.put(type, validator);

    return this;
  }

  /**
   * Returns all the types for which there was a registration for either the reducers or the
   * validators.
   */
  public Stream<String> types() {
    return concat(reducers.keySet().stream(), validators.keySet().stream()).distinct();
  }
}
