package be.lemonade.jes.akka;

import static be.lemonade.jes.akka.JsonFields.COMMAND;
import static be.lemonade.jes.akka.JsonFields.CORR;
import static be.lemonade.jes.akka.JsonFields.ID;
import static be.lemonade.jes.akka.JsonFields.JWT;
import static be.lemonade.jes.akka.JsonFields.OPS;
import static be.lemonade.jes.akka.JsonFields.SEQ;
import static be.lemonade.jes.akka.JsonFields.TYPE;
import static be.lemonade.jes.kafka.Util.createReliableProducer;
import static be.lemonade.jes.kafka.Util.getProducerConfig;
import static be.lemonade.jes.play.Util.createJwtVerifier;
import static java.lang.Integer.MAX_VALUE;
import static java.time.Duration.between;
import static java.time.Instant.now;
import static java.util.concurrent.CompletableFuture.completedFuture;
import static java.util.concurrent.CompletableFuture.runAsync;
import static java.util.stream.Collectors.toSet;
import static javax.json.Json.createArrayBuilder;
import static javax.json.Json.createObjectBuilder;
import static javax.json.Json.createPatch;
import static net.pincette.util.Json.copy;
import static net.pincette.util.Json.emptyObject;
import static net.pincette.util.Json.get;
import static net.pincette.util.Json.hasErrors;
import static net.pincette.util.Json.string;
import static net.pincette.util.Json.transform;
import static net.pincette.util.Pair.pair;
import static net.pincette.util.StreamUtil.range;
import static net.pincette.util.StreamUtil.zip;
import static net.pincette.util.Util.allPaths;
import static net.pincette.util.Util.from;

import akka.actor.ActorRef;
import akka.actor.PoisonPill;
import akka.cluster.sharding.ShardRegion;
import akka.persistence.AbstractPersistentActor;
import akka.persistence.SnapshotOffer;
import com.auth0.jwt.JWTVerifier;
import com.typesafe.config.Config;
import java.time.Duration;
import java.time.Instant;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;
import java.util.stream.Stream;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonValue;
import net.pincette.util.Json;
import net.pincette.util.Json.JsonEntry;
import net.pincette.util.Util.GeneralException;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;

/**
 * An actor that manages aggregate roots which are expressed in JSON.
 *
 * @author Werner Donn\u00e9
 */
public class JsonAggregateRoot extends AbstractPersistentActor {

  private static final String IDLE = "idle";
  private static final String OP = "op";
  private static final String PATH = "path";

  private final KafkaProducer<String, String> eventProducer;
  private final JWTVerifier jwtVerifier;
  private final Duration passivationTimeout;
  private final JsonProcessor reduce;
  private final int snapshotInterval;
  private final String type;
  private final KafkaProducer<String, String> userProducer;
  private final JsonProcessor validate;
  private JsonObject state = emptyObject();
  private Instant touched = now();

  public JsonAggregateRoot(
      final String type,
      final JsonProcessor validate,
      final JsonProcessor reduce,
      final Config config) {
    this.type = type;
    this.validate = validate;
    this.reduce = reduce;
    this.eventProducer = createProducer(getProducerConfig(config));
    this.userProducer = createProducer(getProducerConfig(config));
    this.passivationTimeout =
        config.hasPath("be.lemonade.jes.passivationTimeout")
            ? Duration.ofMillis(config.getLong("be.lemonade.jes.passivationTimeout"))
            : null;
    this.snapshotInterval =
        config.hasPath("be.lemonade.jes.snapshotInterval")
            ? config.getInt("be.lemonade.jes.snapshotInterval")
            : 100;
    this.jwtVerifier = createJwtVerifier(config);
  }

  private static JsonObject addTechnical(final JsonObject newState, final JsonObject event) {
    final JsonObjectBuilder builder =
        createObjectBuilder(newState)
            .add(CORR, event.getString(CORR))
            .add(ID, event.getString(ID))
            .add(TYPE, event.getString(TYPE))
            .add(SEQ, event.getInt(SEQ));

    return event.containsKey(JWT)
        ? builder.add(JWT, event.getString(JWT)).build()
        : builder.build();
  }

  private static boolean appliesToEmbedded(final JsonObject op, final JsonObject embedded) {
    switch (op.getString(OP)) {
      case "remove":
      case "replace":
      case "test":
        return hasPath(embedded, op, PATH);
      case "copy":
      case "move":
        return hasPath(embedded, op, "from");
      default:
        return true;
    }
  }

  private static boolean changes(final JsonObject event) {
    return !event.getJsonArray(OPS).isEmpty();
  }

  private static JsonObject checkEvent(
      final JsonObject oldState, final Set<String> idPaths, final JsonObject event) {
    return Optional.of(event.getJsonArray(OPS).stream())
        .filter(s -> s.noneMatch(op -> isEmbeddedUpdate(idPaths, op.asJsonObject())))
        .map(s -> event)
        .orElseThrow(
            () ->
                new GeneralException(
                    "Embedded update of " + event.getString(ID) + " in " + oldState.getString(ID)));
  }

  private static KafkaProducer<String, String> createProducer(final Map<String, Object> config) {
    return createReliableProducer(config, new StringSerializer(), new StringSerializer());
  }

  private static boolean hasId(final Set<String> idPaths, final String path) {
    return allPaths(path, "/")
        .map(JsonAggregateRoot::siblingId)
        .anyMatch(id -> id.map(idPaths::contains).orElse(false));
  }

  private static boolean hasPath(final JsonObject json, final JsonObject op, final String path) {
    return get(json, toDots(op.getString(path))).isPresent();
  }

  private static boolean isEmbeddedUpdate(final Set<String> idPaths, final JsonObject op) {
    return !"test".equals(op.getString(OP)) && hasId(idPaths, op.getString(PATH));
  }

  private static boolean isFirst(final JsonObject event) {
    return event.getInt(SEQ) == 0;
  }

  private static boolean isGet(final JsonObject command) {
    return Commands.GET.equals(command.getString(COMMAND, ""));
  }

  private static boolean isPassivate(final JsonObject command) {
    return Optional.ofNullable(command.getString(COMMAND, null))
        .map(c -> c.equals(Commands.PASSIVATE))
        .orElse(false);
  }

  private static boolean isUsedEmbedded(final JsonValue value, final JsonObject event) {
    return Optional.of(value)
        .filter(Json::isObject)
        .map(JsonValue::asJsonObject)
        .filter(o -> Util.sameManagedObject(o, event))
        .isPresent();
  }

  private static ProducerRecord<String, String> record(
      final JsonObject json, final String asString, final String topic) {
    return new ProducerRecord<>(topic, json.getString(ID), asString);
  }

  private static JsonObject reduceEvent(final JsonObject event, final JsonObject embedded) {
    return createObjectBuilder(event)
        .add(
            OPS,
            copy(
                event.getJsonArray(OPS),
                createArrayBuilder(),
                op -> appliesToEmbedded(op.asJsonObject(), embedded)))
        .build();
  }

  private static Optional<String> siblingId(final String path) {
    return Optional.of(path.lastIndexOf('/'))
        .filter(index -> index > 0) // The path "/_id" refers to the object itself.
        .map(index -> path.substring(0, index + 1) + ID);
  }

  private static String toDots(final String path) {
    return path.substring(1).replace('/', '.');
  }

  private static Stream<String> combine(final String parent, final Stream<String> children) {
    return children.map(child -> parent + child);
  }

  private static Stream<String> getEmbeddedIdPaths(final JsonValue json) {
    final Supplier<Stream<String>> arrayOf =
        () -> json instanceof JsonArray ? getEmbeddedIdPaths(json.asJsonArray()) : Stream.empty();

    return json instanceof JsonObject ? getEmbeddedIdPaths(json.asJsonObject()) : arrayOf.get();
  }

  private static Stream<String> getEmbeddedIdPaths(final JsonObject json) {
    return json.entrySet()
        .stream()
        .flatMap(e -> pathWithCombinations(e.getKey(), e.getValue()))
        .filter(path -> path.endsWith(ID));
  }

  private static Stream<String> getEmbeddedIdPaths(final JsonArray json) {
    return zip(range(0, MAX_VALUE), json.stream())
        .flatMap(pair -> pathWithCombinations(String.valueOf(pair.first), pair.second));
  }

  private static Stream<String> pathWithCombinations(final String field, final JsonValue value) {
    return Stream.concat(Stream.of("/" + field), combine("/" + field, getEmbeddedIdPaths(value)));
  }

  private CompletionStage<JsonObject> applyBusinessLogic(final JsonObject command) {
    return validate
        .apply(state, command)
        .thenComposeAsync(
            json -> hasErrors(json) ? completedFuture(json) : reduce.apply(state, command));
  }

  private CompletionStage<JsonObject> applyEmbeddedUpdate(final JsonObject event) {
    return completedFuture(
        transform(
            state,
            new Json.Transformer(
                entry -> isUsedEmbedded(entry.value, event), entry -> applyEvent(entry, event))));
  }

  private Optional<JsonEntry> applyEvent(final JsonEntry entry, final JsonObject event) {
    return Optional.of(
        new Json.JsonEntry(
            entry.path,
            Util.applyEvent(
                entry.value.asJsonObject(),
                reduceEvent(event, entry.value.asJsonObject()),
                message -> context().system().log().error(message))));
  }

  public Receive createReceive() {
    return receiveBuilder()
        .matchEquals(IDLE, message -> handleIdle())
        .match(JsonObject.class, this::handleCommand)
        .build();
  }

  public Receive createReceiveRecover() {
    return receiveBuilder()
        .match(SnapshotOffer.class, this::handleSnapshot)
        .match(String.class, this::update)
        .build();
  }

  private Optional<String> getUser(final JsonObject json) {
    return Optional.ofNullable(json.getString(JWT, null))
        .flatMap(jwt -> be.lemonade.jes.play.Util.getUser(jwt, jwtVerifier));
  }

  private void handleCommand(final JsonObject command) {
    if (isPassivate(command)) {
      passivate();
    } else if (Util.isEvent(command) || matches(command)) {
      touched = now();

      Optional.of(handleCommand(command, getSender()))
          .filter(stage -> !isGet(command))
          .ifPresent(stage -> stage.toCompletableFuture().join());
    }
  }

  private CompletionStage<Void> handleCommand(final JsonObject command, final ActorRef sender) {
    return (Util.isEvent(command) ? applyEmbeddedUpdate(command) : applyBusinessLogic(command))
        .thenComposeAsync(
            json ->
                hasErrors(json)
                    ? runAsync(() -> reply(json, sender))
                    : processNewState(json, command, sender));
  }

  private void handleIdle() {
    if (passivationTimeout != null
        && !between(touched, now()).minus(passivationTimeout).isNegative()) {
      passivate();
    }
  }

  private void handleSnapshot(final SnapshotOffer offer) {
    state =
        Json.from((String) offer.snapshot())
            .filter(Json::isObject)
            .map(JsonValue::asJsonObject)
            .map(this::validateSnapshot)
            .orElseThrow(() -> new GeneralException("Corrupt snapshot"));
  }

  private boolean isNext(final JsonObject event) {
    return event.getInt(SEQ) == state.getInt(SEQ) + 1;
  }

  private boolean matches(final JsonObject command) {
    return persistenceId().equals(command.getString(ID, ""))
        && type.equals(command.getString(TYPE, ""));
  }

  private void next() {
    context()
        .system()
        .scheduler()
        .scheduleOnce(
            scala.concurrent.duration.Duration.create(
                passivationTimeout.toMillis(), TimeUnit.MILLISECONDS),
            self(),
            IDLE,
            context().system().dispatcher(),
            null);
  }

  private void passivate() {
    context().parent().tell(new ShardRegion.Passivate(PoisonPill.getInstance()), self());
  }

  public String persistenceId() {
    return self().path().name();
  }

  @Override
  public void postStop() {
    eventProducer.close();
    userProducer.close();
  }

  @Override
  public void preStart() {
    if (passivationTimeout != null) {
      next();
    }
  }

  private void processEvent(
      final JsonObject newState,
      final JsonObject event,
      final JsonObject command,
      final ActorRef sender) {
    if (isGet(command)) {
      sender.tell(newState, self());
    } else {
      if (isFirst(event) || changes(event)) {
        save(addTechnical(newState, event), event, sender);
      } else {
        reply(newState, sender);
      }
    }
  }

  private CompletionStage<Void> processNewState(
      final JsonObject newState, final JsonObject command, final ActorRef sender) {
    return runAsync(
        () ->
            processEvent(
                newState,
                checkEvent(
                    state,
                    getEmbeddedIdPaths(state).collect(toSet()),
                    Util.createEvent(state, newState, command)),
                command,
                sender));
  }

  private void reply(final JsonObject json, final ActorRef sender) {
    final Optional<String> user = getUser(json);

    if (!user.isPresent()) {
      sender.tell(json, self());
    } else {
      userProducer.send(record(json, string(json), user.get()));
      sender.tell(0, self());
    }
  }

  private void save(final JsonObject newState, final JsonObject event, final ActorRef sender) {
    from(string(event))
        .accept(
            ev ->
                persist(
                    ev,
                    e -> {
                      eventProducer.send(record(event, e, TYPE));
                      reply(newState, sender);
                      state = newState;

                      if (event.getInt(SEQ) % snapshotInterval == snapshotInterval - 1) {
                        saveSnapshot(string(state));
                      }
                    }));
  }

  private void update(final String event) {
    state =
        Json.from(event)
            .filter(Json::isObject)
            .map(JsonValue::asJsonObject)
            .map(
                json -> pair(json, createPatch(validateEvent(json).getJsonArray(OPS)).apply(state)))
            .map(pair -> addTechnical(pair.second, pair.first))
            .orElseThrow(() -> new GeneralException("Corrupt event \"" + event + "\""));
  }

  private JsonObject validateEvent(final JsonObject event) {
    if (!persistenceId().equals(event.getString(ID)) && !isNext(event)) {
      throw new GeneralException("Corrupt event " + string(event) + " for ID " + persistenceId());
    }

    return event;
  }

  private JsonObject validateSnapshot(final JsonObject snapshot) {
    if (!persistenceId().equals(snapshot.getString(ID))) {
      throw new GeneralException(
          "Corrupt snapshot " + string(snapshot) + " for ID " + persistenceId());
    }

    return snapshot;
  }
}
