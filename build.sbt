name := """lemonade-jes"""
organization := "be.lemonade"
version := "0.0.3"

scalaVersion := "2.12.4"

libraryDependencies ++= Seq(
  "javax.json" % "javax.json-api" % "1.1.2",
  "com.google.inject" % "guice" % "4.1.0",
  "com.typesafe.akka" %% "akka-actor" % "2.5.11",
  "com.typesafe.akka" %% "akka-persistence" % "2.5.11",
  "com.typesafe.akka" %% "akka-cluster-sharding" % "2.5.11",
  "com.typesafe.akka" %% "akka-stream-kafka" % "0.19",
  "com.typesafe.play" %% "play" % "2.6.12",
  "com.typesafe.play" %% "play-java" % "2.6.12",
  "org.apache.kafka" % "kafka-clients" % "1.0.1",
  "org.mongodb" % "bson" % "3.6.3",
  "com.auth0" % "java-jwt" % "3.3.0",
  "net.pincette" % "pincette-common" % "1.2.3" withSources(),
  "be.lemonade" % "lemonade-jwt" % "0.0.2" withSources()
)

publishTo := Some("Sonatype Nexus Repository Manager" at "https://nexus.lemonade.be/nexus/content/repositories/releases/")
credentials += Credentials(Path.userHome / ".ivy2" / ".credentials")
resolvers += "Pincette" at "https://re.pincette.net/repo"
resolvers += "Sonatype Nexus Repository Manager" at "https://nexus.lemonade.be/nexus/content/repositories/releases"

crossPaths := false
